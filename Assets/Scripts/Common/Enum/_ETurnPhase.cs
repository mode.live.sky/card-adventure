﻿namespace _CA.Common.Enum
{
    public enum _ETurnPhase
    {
        Null,
        Draw,
        Place,
        Attack,
        End
    }
}

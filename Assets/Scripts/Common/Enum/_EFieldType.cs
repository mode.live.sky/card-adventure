﻿namespace _CA.Common.Enum
{
    public enum _EFieldType 
    {
        Null,
        Pile,
        Hand,
        Battlefield,
        Discard
    }
}


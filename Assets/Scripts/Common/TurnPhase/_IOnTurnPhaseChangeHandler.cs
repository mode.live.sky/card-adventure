﻿using _CA.Common.Enum;

namespace _CA.Common.TurnPhase
{
    public interface _IOnTurnPhaseChangeHandler
    {
        void OnChangeTurnPhase(_ETurnPhase currenTurnPhase);
    }
}


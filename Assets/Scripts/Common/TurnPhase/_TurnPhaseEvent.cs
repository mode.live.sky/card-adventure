﻿using System.Collections.Generic;

using _CA.Common.Enum;

namespace _CA.Common.TurnPhase
{
    public static class _TurnPhaseEvent
    {
        public static _ETurnPhase CurrentTurnPhase = _ETurnPhase.Null;

        private static List<_IOnTurnPhaseChangeHandler> _observers = new List<_IOnTurnPhaseChangeHandler>();

        public static void Attach(_IOnTurnPhaseChangeHandler observer)
        {
            _observers.Add(observer);
        }

        public static void Detach(_IOnTurnPhaseChangeHandler observer)
        {
            _observers.Remove(observer);
        }

        public static void Notify()
        {
            foreach (var observer in _observers)
            {
                observer.OnChangeTurnPhase(CurrentTurnPhase);
            }
        }
    }
}

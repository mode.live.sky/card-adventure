﻿using UnityEngine;

using _CA.Common.Enum;

namespace _CA.Common.TurnPhase
{
    public class _TurnPhaseController: MonoBehaviour
    {
        public void _OnDrawCardPhaseClick()
        {
            _TurnPhaseEvent.CurrentTurnPhase = _ETurnPhase.Draw;
            _TurnPhaseEvent.Notify();
        }
        public void _OnPlaceCardPhaseClick()
        {
            _TurnPhaseEvent.CurrentTurnPhase = _ETurnPhase.Place;
            _TurnPhaseEvent.Notify();
        }
        public void _OnBattlePhaseClick()
        {
            _TurnPhaseEvent.CurrentTurnPhase = _ETurnPhase.Attack;
            _TurnPhaseEvent.Notify();
        }
        public void _OnEndTurnPhaseClick()
        {
            _TurnPhaseEvent.CurrentTurnPhase = _ETurnPhase.End;
            _TurnPhaseEvent.Notify();
        }

    }
}

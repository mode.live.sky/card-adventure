﻿using UnityEngine;
using UnityEngine.UI;

using _CA.Common.Enum;
using _CA.Common.TurnPhase;

namespace _CA.Common.Log
{
    public class _LogFieldController: MonoBehaviour, _IOnTurnPhaseChangeHandler
    {
        [SerializeField] Text _logLineReference;
        [SerializeField] Text _phaseLineReference;

        private static Text _logLine;
        private static Text _phaseLine;
       
        private void Awake()
        {
            _logLine = _logLineReference;
            _phaseLine = _phaseLineReference;
        }

        private void OnEnable()
        {
            _TurnPhaseEvent.Attach(this);
        }

        private void OnDisable()
        {
            _TurnPhaseEvent.Detach(this);
        }

        public void OnChangeTurnPhase(_ETurnPhase currentTurnPhase)
        {
            _phaseLine.text = $"Current turn phase: {currentTurnPhase.ToString()}";

            if (currentTurnPhase == _ETurnPhase.End) _logLine.text = "";
        }

        public static void ShowMessage(string message)
        {
            
            _logLine.text = $"{message}\n";
        }

    }
}

﻿using UnityEngine;

using _CA.Cards;
using _CA.Common.GameManager;
using _CA.Cards.Card;

public class GameManager : MonoBehaviour
{
    [SerializeField] private _CardsManager _cardManager;

    private void Awake()
    {
        _GameManager._CameraMain = Camera.main;
        _Card._InjectCardManager(this._cardManager);
    }
}


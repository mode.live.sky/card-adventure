﻿using UnityEngine;

using _CA.Common.TurnPhase;
using _CA.Common.Enum;
using _CA.Fields.Field.Interface;
using _CA.Field;

namespace _CA.Fields.Field
{
    public abstract class _Field: MonoBehaviour, _IField, _IOnTurnPhaseChangeHandler
    {
        protected _ETurnPhase _currentTurnPhase;
        private _IFieldsManager _fieldsManager;

        public _IFieldsManager FieldManager => _fieldsManager;
        public _ETurnPhase GetCurrentTurnPhase() => _currentTurnPhase;
        public virtual void TransferCard(Transform card)
        {
            card.SetParent(transform);
            card.position = transform.position;
        }
        public void InjectFieldManagerReference(_IFieldsManager fieldManager)
        {
            _fieldsManager = fieldManager;
        }

        private void OnEnable()
        {
            _TurnPhaseEvent.Attach(this);
        }
        private void OnDisable()
        {
            _TurnPhaseEvent.Detach(this);
        }
        public void OnChangeTurnPhase(_ETurnPhase currentTurnPhase)
        {
            _currentTurnPhase = currentTurnPhase;
        }
    }
}
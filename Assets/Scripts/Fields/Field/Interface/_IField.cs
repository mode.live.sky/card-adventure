﻿using UnityEngine;

using _CA.Common.Enum;
using _CA.Field;

namespace _CA.Fields.Field.Interface
{
    public interface _IField
    {
        _IFieldsManager FieldManager { get; }
        _ETurnPhase GetCurrentTurnPhase();

        void TransferCard(Transform card);
        void InjectFieldManagerReference(_IFieldsManager fieldManager);
    }
}
﻿using UnityEngine;

using _CA.Common.Enum;
using _CA.Common.Log;
using _CA.Fields.Field;
using _CA.Fields.Pile.Interface;

namespace _CA.Fields.Pile
{
    public class _PileFieldController: _Field, _IPileFieldController
    {
        public void GetNextCard()
        {
            if (CheckConditionsToPassDrawCard() == false) return;

            var creatureCardGameObject = _getCreaturePrefabInstantiate();
            base.FieldManager.HandFieldControllerReference.TransferCard(creatureCardGameObject.transform);

            _LogFieldController.ShowMessage("Draw card action was done.");
        }
        private bool CheckConditionsToPassDrawCard()
        {
            if (base.GetCurrentTurnPhase() != _ETurnPhase.Draw)
            {
                _LogFieldController.ShowMessage("This is not 'Draw' turn's phase!");
                return false;
            }
            if (base.FieldManager.HandFieldControllerReference.CheckIfThereAnyFreeSlot() == false)
            {
                _LogFieldController.ShowMessage("There are no free slots on hand!");
                return false;
            }
            return true;
        }

        private GameObject _getCreaturePrefabInstantiate()
        {
            var creatureCardPrefab = base.FieldManager.CreatureCardPrefab;
            var creatureCardGameObject = Instantiate(creatureCardPrefab);
            return creatureCardGameObject;
        }
    }
}

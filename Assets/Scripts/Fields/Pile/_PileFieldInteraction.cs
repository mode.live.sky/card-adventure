﻿using UnityEngine;
using UnityEngine.EventSystems;

using _CA.Fields.Pile.Interface;

namespace _CA.Fields.Pile
{
    [RequireComponent(typeof(_IPileFieldController))]
    public class _PileFieldInteraction : MonoBehaviour, IPointerClickHandler
    {
        private _IPileFieldController _pileFieldController;

        private void Awake()
        {
            _pileFieldController = GetComponent<_IPileFieldController>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _pileFieldController.GetNextCard();
        }
    }
}

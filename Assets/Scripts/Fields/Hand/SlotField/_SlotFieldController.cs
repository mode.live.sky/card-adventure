﻿using UnityEngine;

namespace _CA.Fields.Hand.Slot
{
    public class _SlotFieldController: MonoBehaviour, _ISlotFieldController
    {
        [Header("Parent slot reference for the card.")]
        [Space]
        [SerializeField] Transform _parentForSlotCard;
        public Transform ParentForSlotCard => _parentForSlotCard;

        private int _getChildNumber => _parentForSlotCard.childCount;

        public bool IsThereAnySlotFree
        {
            get
            {
                return _getChildNumber >= 1 ? false : true;
            }
        }

    }
}

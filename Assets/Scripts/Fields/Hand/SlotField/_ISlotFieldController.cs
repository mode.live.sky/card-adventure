﻿using UnityEngine;

namespace _CA.Fields.Hand.Slot
{
    public interface _ISlotFieldController
    {
        bool IsThereAnySlotFree { get; }
        Transform ParentForSlotCard { get; }
    }
}
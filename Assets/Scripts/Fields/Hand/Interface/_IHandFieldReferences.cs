﻿using System.Collections.Generic;

using _CA.Fields.Hand.Slot;

namespace _CA.Field.Hand.Interface
{
    public interface _IHandFieldReferences
    {
        List<_SlotFieldController> SlotFieldControllerReferences { get; }
    }
}
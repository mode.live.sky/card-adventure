﻿using UnityEngine;

namespace _CA.Fields.Hand.Interface
{
    public interface _IHandFieldController
    {
        void TransferCard(Transform card);
        bool CheckIfThereAnyFreeSlot();
    }
}
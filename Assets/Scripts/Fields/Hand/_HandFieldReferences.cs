﻿using System.Collections.Generic;
using UnityEngine;

using _CA.Fields.Hand.Slot;
using _CA.Field.Hand.Interface;

namespace _CA.Fields.Hand
{
    public class _HandFieldReferences: MonoBehaviour, _IHandFieldReferences
    {
        [Header("References to hand's slots.")]
        [Space()]
        [SerializeField] List<_SlotFieldController> _slotFieldControllerReferences;
        public List<_SlotFieldController> SlotFieldControllerReferences => _slotFieldControllerReferences;
    }
}

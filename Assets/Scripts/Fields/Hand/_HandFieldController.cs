﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using _CA.Fields.Hand.Slot;
using _CA.Field.Hand.Interface;
using _CA.Fields.Field;
using _CA.Fields.Hand.Interface;

namespace _CA.Fields.Hand
{
    [RequireComponent(typeof(_IHandFieldReferences))]
    public class _HandFieldController: _Field, _IHandFieldController
    {
        private _IHandFieldReferences _handFieldReferences;
        private List<_ISlotFieldController> _freeSlotList
        {
            get
            {
                var slots = _handFieldReferences.SlotFieldControllerReferences;
                var freeslot = from item in slots where item.IsThereAnySlotFree == true select item;
                return freeslot.ToList<_ISlotFieldController>();
            }

        }
        private _ISlotFieldController _getFreeSlotFieldReference() => _freeSlotList.FirstOrDefault();

        private void Awake()
        {
            _handFieldReferences = GetComponent<_IHandFieldReferences>();
        }
        public override void TransferCard(Transform card)
        {
            var parentForCard = _getFreeSlotFieldReference().ParentForSlotCard;
            card.SetParent(parentForCard);
            card.position = parentForCard.position;
        }

        public bool CheckIfThereAnyFreeSlot()
        {
            return _freeSlotList.Count() > 0 ? true : false;
        }
    }
}


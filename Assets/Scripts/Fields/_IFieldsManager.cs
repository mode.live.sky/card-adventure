﻿using UnityEngine;

using _CA.Fields.Hand.Interface;

namespace _CA.Field
{
    public interface _IFieldsManager
    {
        GameObject PileFieldReference { get; }
        GameObject HandFieldReference { get; }
        _IHandFieldController HandFieldControllerReference { get; }
        GameObject CreatureCardPrefab { get; }
    }
}
﻿using UnityEngine;

using _CA.Fields.Battle.Interface;

namespace _CA.Fields.Battle
{
    public class _BattleFieldReferences: MonoBehaviour, _IBattleFieldReferences
    {
        [Header("Parent reference of cards on battle field.")]
        [Space()]
        [SerializeField] Transform _onBattleFieldCardsParent;
        public Transform OnBattleFieldCardsParent => _onBattleFieldCardsParent;
    }
}

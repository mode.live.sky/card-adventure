﻿using UnityEngine;

namespace _CA.Fields.Battle.Interface
{
    public interface _IBattleFieldReferences
    {
        Transform OnBattleFieldCardsParent { get; }
    }
}
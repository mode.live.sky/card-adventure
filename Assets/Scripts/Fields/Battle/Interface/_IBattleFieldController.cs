﻿using UnityEngine;

namespace _CA.Fields.Battle.Interface
{
    public interface _IBattleFieldController
    {
        bool CheckIfCardCanBeTransfered();
        void TransferCard(Transform card);
        void OrderCreatureCardToAttack();
    }
}
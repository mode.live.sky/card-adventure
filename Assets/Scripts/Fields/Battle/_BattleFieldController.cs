﻿using UnityEngine;

using _CA.Common.Log;
using _CA.Common.Enum;
using _CA.Common.TurnPhase;
using _CA.Fields.Field;
using _CA.Fields.Battle.Interface;
using _CA.Cards.Creature.Interface;

namespace _CA.Fields.Battle
{
    [RequireComponent(typeof(_IBattleFieldReferences))]
    public class _BattleFieldController: _Field, _IBattleFieldController, _IOnTurnPhaseChangeHandler
    {
        private _IBattleFieldReferences _battleFieldReferences;

        private void Awake()
        {
            _battleFieldReferences = GetComponent<_IBattleFieldReferences>();
        }

        public bool CheckIfCardCanBeTransfered()
        {
            if (base.GetCurrentTurnPhase() != _ETurnPhase.Place)
            {
                _LogFieldController.ShowMessage("This is not 'Place' turn's phase!");
                return false;
            }
            if(_battleFieldReferences.OnBattleFieldCardsParent.childCount > 0)
            {
                _LogFieldController.ShowMessage("This is a creature on battlefield already!");
                return false;
            }
            return true;
        }

        public override void TransferCard(Transform card)
        {
            var parentForCard = _battleFieldReferences.OnBattleFieldCardsParent;

            card.SetParent(parentForCard);
            card.position = parentForCard.position;

            _LogFieldController.ShowMessage("Creature card was transfered to battlefield.");
        }

        public void OrderCreatureCardToAttack()
        {
            if (_battleFieldReferences.OnBattleFieldCardsParent.childCount <= 0)
            {
                _LogFieldController.ShowMessage("This is a no creature on battlefield!");
                return;
            }

            var creatureCardController = _battleFieldReferences.OnBattleFieldCardsParent.GetComponentInChildren<_ICreatureCardController>();
            creatureCardController._OrderToAttack();
        }

        public new void OnChangeTurnPhase(_ETurnPhase currentTurnPhase)
        {
            base.OnChangeTurnPhase(currentTurnPhase);
            if (currentTurnPhase == _ETurnPhase.Attack)
            OrderCreatureCardToAttack();
        }
    }
}

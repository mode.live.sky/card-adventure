﻿using UnityEngine;
using UnityEngine.EventSystems;

using _CA.Common.Struct;
using _CA.Fields.Battle.Interface;

namespace _CA.Fields.Battle
{
    [RequireComponent(typeof(_IBattleFieldController))]
    public class _BattleFieldInteraction: MonoBehaviour, IDropHandler
    {
        private _IBattleFieldController _battleFieldController;

        private void Awake()
        {
            _battleFieldController = GetComponent<_IBattleFieldController>();
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData?.pointerDrag == null) return;

            var item = eventData.pointerDrag;

            if (item.CompareTag(_STagNames.TagCreatureName))
            {
                if (_battleFieldController.CheckIfCardCanBeTransfered() == false) return;
                
                _battleFieldController.TransferCard(item.transform);
            }
        }
    }
}

﻿using UnityEngine;

using _CA.Cards.Card.Interface;
using _CA.Common.Enum;
using _CA.Common.Log;
using _CA.Fields.Field;

namespace _CA.Fields.Discard
{
    public class _DiscardFieldController: _Field, _IDiscardFieldController
    {
        public override void TransferCard(Transform card)
        {
            base.TransferCard(card);

            _ICard controller = card.GetComponent<_ICard>();
            controller._SetCardCurrentField(_EFieldType.Discard);
            controller._DiscardCard();

            _LogFieldController.ShowMessage("Creature card was discarded.");
        }

        public bool CheckIfCardCanBeTransfered()
        {
            return base.GetCurrentTurnPhase() == _ETurnPhase.Place;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.EventSystems;

using _CA.Common.Log;
using _CA.Common.Struct;
using _CA.Common.Enum;

namespace _CA.Fields.Discard
{
    [RequireComponent(typeof(_IDiscardFieldController))]
    public class _DiscardFieldInteraction: MonoBehaviour, IDropHandler
    {
        private _IDiscardFieldController _discardFieldController;

        private void Awake()
        {
            _discardFieldController = GetComponent<_IDiscardFieldController>();
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData?.pointerDrag == null) return;
            
            var item = eventData.pointerDrag;

            if (item.CompareTag(_STagNames.TagCreatureName))
            {
                if (_discardFieldController.CheckIfCardCanBeTransfered() == false) return;

                _discardFieldController.TransferCard(item.transform);
                return;
            }
        }
    }
}

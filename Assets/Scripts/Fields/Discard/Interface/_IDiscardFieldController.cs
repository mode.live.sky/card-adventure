﻿using UnityEngine;

namespace _CA.Fields.Discard
{
    public interface _IDiscardFieldController
    {
        bool CheckIfCardCanBeTransfered();
        void TransferCard(Transform card);
    }
}
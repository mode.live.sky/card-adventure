﻿using UnityEngine;

using _CA.Fields.Pile;
using _CA.Fields.Hand;
using _CA.Fields.Battle;
using _CA.Fields.Pile.Interface;
using _CA.Fields.Hand.Interface;
using _CA.Fields.Battle.Interface;
using _CA.Fields.Field.Interface;

namespace _CA.Field
{
    public class _FieldsManager : MonoBehaviour, _IFieldsManager
    {
        [Header("Pile field reference.")]
        [Space()]
        [SerializeField] GameObject _pileFieldReference;
        public GameObject PileFieldReference => _pileFieldReference;

        private _IPileFieldController _pileFieldControllerReference;
        public _IPileFieldController PileFieldControllerReference => _pileFieldControllerReference;

        [Header("Hand field reference.")]
        [Space()]
        [SerializeField] GameObject _handFieldReference;
        public GameObject HandFieldReference => _handFieldReference;

        private _IHandFieldController _handFieldControllerReference;
        public _IHandFieldController HandFieldControllerReference => _handFieldControllerReference;


        [Header("Battle field reference.")]
        [Space()]
        [SerializeField] GameObject _battleFieldReference;
        public GameObject BattleFieldReference => _battleFieldReference;

        private _IBattleFieldController _battleFieldControllerReference;
        public _IBattleFieldController BattleFieldControllerReference => _battleFieldControllerReference;


        [Header("Creature card reference.")]
        [Space()]
        [SerializeField] GameObject _creatureCardPrefab;
        public GameObject CreatureCardPrefab => _creatureCardPrefab;

        private void Awake()
        {
            _pileFieldControllerReference = _pileFieldReference.GetComponent<_IPileFieldController>();
            _handFieldControllerReference = _handFieldReference.GetComponent<_IHandFieldController>();
            _battleFieldControllerReference = _battleFieldReference.GetComponent<_IBattleFieldController>();
        }

        private void Start()
        {
            _pileFieldReference.GetComponent<_IField>().InjectFieldManagerReference(this);
            _handFieldReference.GetComponent<_IField>().InjectFieldManagerReference(this);
            _battleFieldReference.GetComponent<_IField>().InjectFieldManagerReference(this);
        }
    }
}

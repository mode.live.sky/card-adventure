using UnityEngine;
using _CA.Common.TurnPhase;

namespace _CardAdventure.Scripts.TurnsPhase
{
    public class TurnsPhaseController : MonoBehaviour
    {
        [SerializeField] private _TurnPhaseController _turnPhaseController;

        private readonly string _buttonDrawCoordinatesName = "Draw";
        private readonly Rect _buttonDrawCoordinates = new Rect(10, 190, 70, 70);

        private readonly string _buttonPlaceCoordinatesName = "Place";
        private readonly Rect _buttonPlaceCoordinates = new Rect(10, 265, 70, 70);

        private readonly string _buttonBattleCoordinatesName = "Battle";
        private readonly Rect _buttonBattleCoordinates = new Rect(10, 340, 70, 70);

        private readonly string _buttonEndTurnCoordinatesName = "End turn";
        private readonly Rect _buttonEndTurnCoordinates = new Rect(10, 415, 70, 70);


        private void OnGUI()
        {
            if (GUI.Button(this._buttonDrawCoordinates, this._buttonDrawCoordinatesName))
            {
                this._turnPhaseController._OnDrawCardPhaseClick();
            }

            if (GUI.Button(this._buttonPlaceCoordinates, this._buttonPlaceCoordinatesName))
            {
                this._turnPhaseController._OnPlaceCardPhaseClick();
            }

            if (GUI.Button(this._buttonBattleCoordinates, this._buttonBattleCoordinatesName))
            {
                this._turnPhaseController._OnBattlePhaseClick();
            }

            if (GUI.Button(this._buttonEndTurnCoordinates, this._buttonEndTurnCoordinatesName))
            {
                this._turnPhaseController._OnEndTurnPhaseClick();
            }
        }
    }
}
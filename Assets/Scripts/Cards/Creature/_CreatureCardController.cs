﻿using _CA.Cards.Card;
using _CA.Cards.Creature.Interface;
using _CA.Common.Log;
using _CA.Scriptable;

namespace _CA.Cards.Creature
{
    public class _CreatureCardController : _Card, _ICreatureCardController
    {
        private _ICreatureCardReferences _cardCreatureReference;
        private _CreatureCardData _creatureCardData;


        private void Awake()
        {
            this._cardCreatureReference = GetComponentInChildren<_ICreatureCardReferences>();
        }

        private void Start()
        {
            this._prepareCreatureCard();
        }

        public void _OrderToAttack()
        {
            _LogFieldController.ShowMessage($"The creature has attacked, {_creatureCardData.Attack} damage was given.");
        }

        private void _prepareCreatureCard()
        {
            
            this._creatureCardData = _CardsManager.GetNextCreatureCardData();

            this._cardCreatureReference._PortraitReference.sprite = this._creatureCardData.Portrait;
            this._cardCreatureReference._NameTextReference.text = this._creatureCardData.name;
            this._cardCreatureReference._LifeTextReference.text = this._creatureCardData.Life.ToString();
            this._cardCreatureReference._AttackTextReference.text = this._creatureCardData.Attack.ToString();
        }
    }
}

﻿using UnityEngine.UI;

namespace _CA.Cards.Creature.Interface
{
    public interface _ICreatureCardReferences
    {
        Image _PortraitReference { get; }
        Text _NameTextReference { get; }
        Text _LifeTextReference { get; }
        Text _AttackTextReference { get; }
    }
}



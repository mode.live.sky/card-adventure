﻿using UnityEngine;
using UnityEngine.UI;

using _CA.Cards.Creature.Interface;

namespace _CA.Cards.Creature
{
    public class _CreatureCardReferences : MonoBehaviour, _ICreatureCardReferences
    {
        [SerializeField] Image _portrait;
        public Image _PortraitReference => _portrait;

        [SerializeField] Text _nameText;
        public Text _NameTextReference => _nameText;

        [SerializeField] Text _lifeText;
        public Text _LifeTextReference => _lifeText;

        [SerializeField] Text _attackText;
        public Text _AttackTextReference => _attackText;
    }
}

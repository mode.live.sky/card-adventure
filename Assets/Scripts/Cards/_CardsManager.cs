﻿using System.Collections.Generic;
using UnityEngine;

using _CA.Scriptable;

namespace _CA.Cards
{
    public class _CardsManager : MonoBehaviour, _ICardsManager
    {
        [Header("Table of creatures data (Scriptable).")]
        [SerializeField] private List<_CreatureCardData> _creatureCardData;

        private List<_CreatureCardData> _creatureCardDataClone = new List<_CreatureCardData>();
        public List<_CreatureCardData> _CreatureCardData => _creatureCardDataClone;


        private void Awake()
        {
            foreach (var item in _creatureCardData)
            {
                this._creatureCardDataClone.Add(Instantiate(item));
            }
        }

        public _CreatureCardData GetNextCreatureCardData()
        {
            int index = Random.Range(0, this._CreatureCardData.Count);
            return this._CreatureCardData[index];
        }
    }
}

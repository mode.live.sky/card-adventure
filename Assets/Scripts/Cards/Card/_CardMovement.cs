﻿using UnityEngine;
using UnityEngine.EventSystems;

using _CA.Cards.Card.Interface;
using _CA.Common.Enum;
using _CA.Common.GameManager;

namespace _CA.Cards.Card
{
    [RequireComponent(typeof(_ICard))]
    [RequireComponent(typeof(CanvasGroup))]
    public class _CardMovement : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        private Camera _camera;
        private CanvasGroup _canvasGroup;

        private _ICard _cardController;

        private _EFieldType _fieldTypeBeforeBeginDrag = _EFieldType.Null;


        private void Awake()
        {
            this._cardController = GetComponent<_ICard>();
            
            this._camera = _GameManager._CameraMain;
            this._canvasGroup = GetComponent<CanvasGroup>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (this._cardController._CanCardMove == false) return;

            this._canvasGroup.blocksRaycasts = false;

            if (Input.GetMouseButton(0))
            {
                this._fieldTypeBeforeBeginDrag = this._cardController._CardCurrentField;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (this._cardController._CanCardMove == false) return;
            if (eventData?.pointerDrag == null) return;

            if (Input.GetMouseButton(0))
            {
                Vector3 data = this._camera.ScreenToWorldPoint(Input.mousePosition);
                this.transform.position = new Vector3(data.x, data.y, 0f);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (this._cardController._CanCardMove == false) return;

            this._canvasGroup.blocksRaycasts = true;

            if (this._fieldTypeBeforeBeginDrag == _cardController._CardCurrentField)
            {
                this.transform.position = this.transform.parent.position;
            }
        }
    }
}

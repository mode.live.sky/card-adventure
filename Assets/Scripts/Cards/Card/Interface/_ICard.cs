﻿using _CA.Common.Enum;

namespace _CA.Cards.Card.Interface
{
    public interface _ICard
    {
        bool _CanCardMove { get; }
        _EFieldType _CardCurrentField { get; }

        void _SetCardCurrentField(_EFieldType field);
        void _DiscardCard();
    }
}


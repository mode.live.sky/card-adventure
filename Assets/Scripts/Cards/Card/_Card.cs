﻿using UnityEngine;

using _CA.Cards.Card.Interface;

using _CA.Common.Enum;
using _CA.Common.TurnPhase;

namespace _CA.Cards.Card
{
 //   [RequireComponent(typeof(_CardMovement))]
    public abstract class _Card : MonoBehaviour, _ICard, _IOnTurnPhaseChangeHandler
    {
        public static _CardsManager _CardsManager { get; private set; }

        public bool _CanCardMove => _canCardMove;
        public _EFieldType _CardCurrentField => _cardCurrentField;

        private bool _canCardMove = false;
        private _EFieldType _cardCurrentField;

        public void _SetCardCurrentField(_EFieldType field)
        {
            this._cardCurrentField = field;
        }

        public void _DiscardCard()
        {
            GameObject.Destroy(this.gameObject);
        }

        private void OnEnable()
        {
            _TurnPhaseEvent.Attach(this);
        }

        private void OnDisable()
        {
            _TurnPhaseEvent.Detach(this);
        }

        public void OnChangeTurnPhase(_ETurnPhase currentTurnPhase)
        {
            this._canCardMove = false;

            if (currentTurnPhase == _ETurnPhase.Place)
            {
                this._canCardMove = true;
            }
        }

        public static void _InjectCardManager(_CardsManager cardManager)
        {
            _CardsManager = cardManager;
        }
    }
}

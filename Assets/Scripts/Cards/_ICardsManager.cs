﻿using System.Collections.Generic;

using _CA.Scriptable;

namespace _CA.Cards
{
    public interface _ICardsManager
    {
        List<_CreatureCardData> _CreatureCardData { get; }
        _CreatureCardData GetNextCreatureCardData();
    }
}
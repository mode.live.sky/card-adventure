using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Tests
{
    public class VeryFirstTests
    {
        [Test]
        public void VeryFirstTestsSimplePasses()
        {
            Assert.Pass();
        }

        [UnityTest]
        public IEnumerator VeryFirstTestsWithEnumeratorPasses()
        {
            yield return null;
            Assert.Pass();
        }
    }
}
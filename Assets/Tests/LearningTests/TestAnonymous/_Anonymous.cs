using System;
using UnityEngine;

namespace _CA.Tests.LearningTests.TestAnonymous
{
    public class _Anonymous : MonoBehaviour
    {
        private void Start()
        {
            this._AnonymousRunner1(() => { Debug.Log($"_AnonymousRunner1 works!"); });

            this._AnonymousRunner2((value) => { Debug.Log($"_AnonymousRunner2 calue = {++value}"); });

            var runner3 = this._AnonymousRunner3(() => { return 30; });
            Debug.Log($"_AnonymousRunner3 = {runner3} / {runner3.GetType()}");

            var runner4 = this._AnonymousRunner4((v) => { return (float)v * v; });
            Debug.Log($"_AnonymousRunner4 = {runner4} / {runner4.GetType()}");
        }

        public void _AnonymousRunner1(Action action)
        {
            action?.Invoke();
        }

        public void _AnonymousRunner2(Action<int> action)
        {
            action?.Invoke(10);
        }

        public int _AnonymousRunner3(Func<int> func)
        {
            return func.Invoke();
        }

        public float _AnonymousRunner4(Func<int, float> func)
        {
            return func.Invoke(20);
        }
    }
}
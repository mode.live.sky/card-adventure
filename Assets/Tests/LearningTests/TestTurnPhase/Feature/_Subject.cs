using System;

using Random = UnityEngine.Random;

namespace _CA.Tests.LearningTests.TestTurnPhase.Feature
{
    public static class _Subject<T> where T : Enum
    {
        public static event Action<T> _Subscribers;

        public static void _Notify(T data)
        {
            _Subject<T>._Subscribers?.Invoke(data);
        }
    }
}
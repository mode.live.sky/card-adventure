using UnityEngine;

namespace _CA.Tests.LearningTests.TestTurnPhase.Feature
{
    public class _TriggerActivator : MonoBehaviour
    {
        [ContextMenu("_Activate")]
        public void _Notify()
        {
            _ETurnPhase currentTurn = (_ETurnPhase)Random.Range(0, 5);

            _Subject<_ETurnPhase>._Notify(currentTurn);
        }
    }
}
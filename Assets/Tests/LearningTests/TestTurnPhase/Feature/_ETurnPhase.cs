using UnityEngine;

namespace _CA.Tests.LearningTests.TestTurnPhase.Feature
{
    public enum _ETurnPhase
    {
        Null,
        Draw,
        Place,
        Battle,
        End
    }
}
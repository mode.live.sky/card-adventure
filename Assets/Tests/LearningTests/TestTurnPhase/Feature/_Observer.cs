using UnityEngine;

namespace _CA.Tests.LearningTests.TestTurnPhase.Feature
{
    public class _Observer : MonoBehaviour
    {
        private _ETurnPhase _currentPhase;

        private void OnEnable()
        {
            _Subject<_ETurnPhase>._Subscribers += _onPhaseChange;
        }

        private void _onPhaseChange(_ETurnPhase currentPhase)
        {
            this._currentPhase = currentPhase;

            Debug.Log($"On Trigger for {transform.name} = {this._currentPhase}");
        }

        private void OnDisable()
        {
            _Subject<_ETurnPhase>._Subscribers -= _onPhaseChange;
        }
    }
}
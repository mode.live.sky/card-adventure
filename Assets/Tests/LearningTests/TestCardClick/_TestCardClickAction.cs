using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestCardClick
{
    public class _TestCardClickAction : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            if(eventData.button == PointerEventData.InputButton.Left)
            {
                Debug.Log("Left click.");
            }
            if(eventData.button == PointerEventData.InputButton.Middle)
            {
                Debug.Log("Middle click.");
            }

            if(eventData.button == PointerEventData.InputButton.Right)
            {
                Debug.Log("Right click.");
            }
        }
    }
}
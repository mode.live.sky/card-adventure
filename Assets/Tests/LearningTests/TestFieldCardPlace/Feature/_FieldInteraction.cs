using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestFieldCardPlace.Feature
{
    public class _FieldInteraction : MonoBehaviour, IDropHandler
    {
        public void OnDrop(PointerEventData eventData)
        {
            if (eventData?.pointerDrag == null)
            {
                return;
            }

            Debug.Log(
                $"The {this.transform.name} gets {eventData.pointerDrag.name}.");

            GameObject dropObject = eventData.pointerDrag;

            dropObject.transform.position = this.transform.position;
            dropObject.transform.parent = this.transform;
        }
    }
}
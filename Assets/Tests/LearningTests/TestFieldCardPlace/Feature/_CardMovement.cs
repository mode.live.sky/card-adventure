using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestFieldCardPlace.Feature
{
    public class _CardMovement : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        private Camera _camera;
        private CanvasGroup _canvasGroup;

        private void Awake()
        {
            this._camera = Camera.main;
            this._canvasGroup = this.GetComponent<CanvasGroup>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData?.pointerDrag == null)
            {
                return;
            }
            Vector3 cursorPosition = this._camera.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(cursorPosition.x, cursorPosition.y, 0f);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = true;
        }
    }
}
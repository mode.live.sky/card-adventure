namespace _CA.Tests.LearningTests.TestFieldCardPlace.TestTransferCard
{
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class _Field : MonoBehaviour, IDropHandler
    {
        private const string _cardTag = "CreatureCard";

        public void OnDrop(PointerEventData eventData)
        {
            if (!eventData?.pointerDrag)
            {
                Debug.Log($"Null?");
                return;
            }

            if (!eventData.pointerDrag.CompareTag(_Field._cardTag))
            {
                Debug.Log($"Bad tag = {eventData.pointerDrag.tag}");
                return;
            }

            _Strategy._DoTransfer(this, eventData.pointerDrag.transform);

            if (!new _CardChain(eventData.pointerDrag)
                ._IsCard()
                ._CanCardMove()
                ._CanCardBeMoved()
                ._Check)
            {
                return;
            }
        }

        public class _CardChain
        {
            private bool _effect = true;
            private GameObject _obj;

            public bool _Check
            {
                get
                {
                    return this._effect;
                }
            }

            public _CardChain(GameObject obj)
            {
                this._obj = obj;
            }

            public _CardChain _IsCard()
            {
                if (!this._effect)
                {
                    return this;
                }

                this._effect = _TagAccepters._GetTagFromString(_obj.tag) == _Tag.Card ? true : false;

                return this;
            }

            public _CardChain _CanCardMove()
            {
                if (!this._effect)
                {
                    return this;
                }

                // logic

                return this;
            }

            public _CardChain _CanCardBeMoved()
            {
                if (!this._effect)
                {
                    return this;
                }

                // logic

                return this;
            }
        }
    }
}
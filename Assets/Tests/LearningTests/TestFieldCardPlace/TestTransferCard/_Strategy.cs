using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _CA.Tests.LearningTests.TestFieldCardPlace.TestTransferCard
{
    // _Pobieram tag?
    // _StringtoEnumTag() // b�d� potrzebowa� takiej metody
    // _Strategy._DoSomethingDependsOnTag(_Tag);

    public class _Strategy : MonoBehaviour
    {
        public static IEnumerable<_IStrategy> _strategies
        {
            get
            {
                yield return new _CardStrategy();
            }
        }
        public static void _DoTransfer(_Field field, Transform card)
        {
            _Tag tag = _TagAccepters._GetTagFromString(card.tag);

            _IStrategy currentStrategy = _findStrategy(tag);

            currentStrategy?._DoTask(field.transform, card);
        }

        private static _IStrategy _findStrategy(_Tag tag)
        {

            foreach (_IStrategy item in _Strategy._strategies)
            {
                if (item._Accepts(tag))
                {
                    return item;
                }
            }

            return null;
        }
    }

    public abstract class _AStrategy 
    {
        protected _Tag _AcceptedTag;

        public _AStrategy(_Tag acceptedTag)
        {
            this._AcceptedTag = acceptedTag;
        }

        public virtual bool _Accepts(_Tag tag)
        {
            return tag == this._AcceptedTag;
        }
        public virtual void _DoTask(Transform parent, Transform child)
        {
            child.position = parent.position;
            child.parent = parent.transform;
        }
    }

    public interface _IStrategy
    {
        bool _Accepts(_Tag tag);
        void _DoTask(Transform parent, Transform child);
    }

    public class _CardStrategy : _AStrategy, _IStrategy
    {
        public _CardStrategy() : base(_Tag.Card)
        {
        }
    }

    public static class _TagAccepters
    {
        private static IEnumerable<_TagMatch> _getTags
        {
            get
            {
                yield return new("CreatureCard", _Tag.Card);
                yield return new("Energy", _Tag.Energy);
            }
        }

        public static _Tag _GetTagFromString(string tag)
        {
            //   dynamic tagMatch = _TagAccepters._getTags
            //       .FirstOrDefault(item => item._TagString == tag);
            //   return tagMatch._Tag ??= _Tag.Null;;

            foreach (var item in _TagAccepters._getTags)
            {
                if (item._TagString == tag)
                {
                    return item._Tag;
                }
            }
            return _Tag.Null;
        }
    }

    public readonly struct _TagMatch
    {
        public readonly string _TagString;
        public readonly _Tag _Tag;

        public _TagMatch(string tagString, _Tag tag)
        {
            _TagString = tagString;
            _Tag = tag;
        }
    }

    public enum _Tag
    {
        Null,
        Card,
        Energy
    }
}
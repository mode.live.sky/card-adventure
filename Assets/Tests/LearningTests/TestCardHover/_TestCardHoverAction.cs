using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _CA.Tests.LearningTests.TestCardHover
{
    public class _TestCardHoverAction : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private Image _image;
        private Color _orignalColor;


        private void Awake()
        {
            this._image = this.GetComponentInChildren<Image>();
            this._orignalColor = this._image.color;
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            Debug.Log($"OnPointerEnter");
            
            this._image.color = Color.green;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Debug.Log($"OnPointerExit");

            this._image.color = this._orignalColor;
        }
    }
}
using System.Collections;
using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardPooling.RawFeature
{
    public class _PooledObject : MonoBehaviour
    {
        private _ObjectPool _pool;
        public _ObjectPool _Pool
        {
            get => this._pool;
            set => this._pool = value;
        }

        public void _Release()
        {
            this._pool._Release(this);
        }

        private void OnEnable()
        {
            var r = this.transform.GetComponentInChildren<Renderer>();
            r.material.color = Random.ColorHSV();

            var p = Random.Range(-500f, 500f);
            this.transform.position = new Vector3(p, p, 0.5f);

            StartCoroutine(DeactivateRoutine(1f));
        }

        IEnumerator DeactivateRoutine(float delay)
        {
            yield return new WaitForSeconds(delay);

            OnDisable();
        }

        private void OnDisable()
        {
            this._pool._Release(this);
        }
    }
}
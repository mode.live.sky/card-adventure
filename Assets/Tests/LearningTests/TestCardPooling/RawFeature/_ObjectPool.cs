using System.Collections.Generic;
using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardPooling.RawFeature
{
    public class _ObjectPool : MonoBehaviour
    {
        [SerializeField] private uint _initialPoolSize;
        [SerializeField] private _PooledObject _objectToPool;

        private Stack<_PooledObject> _stack;

        private void Start()
        {
            this._setupPool();
        }

        public _PooledObject _Get()
        {
            _PooledObject instance = null;
            if (this._stack.Count == 0)
            {
                instance = GameObject.Instantiate(this._objectToPool);
                instance._Pool = this;

                return instance;
            }

            instance = this._stack.Pop();
            instance.gameObject.SetActive(true);

            return instance;
        }

        public void _Release(_PooledObject gameObject)
        {
            this._stack.Push(gameObject);
            gameObject.gameObject.SetActive(false);
        }


        private void _setupPool()
        {
            this._stack = new Stack<_PooledObject>();

            _PooledObject instance = null;

            for (int i = 0; i < this._initialPoolSize; i++)
            {
                instance = GameObject.Instantiate(this._objectToPool);
                instance._Pool = this;
                instance.gameObject.SetActive(false);
                this._stack.Push(instance);
            }
        }
    }
}
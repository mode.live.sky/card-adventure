using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardPooling.RawFeature
{
    public class _ObjectPoolController : MonoBehaviour
    {
        [SerializeField] private _ObjectPool _ObjectPool;
        [SerializeField] private float _calmdown = 50f;

        private float _countDown = 0f;

        private void FixedUpdate()
        {
            if (this._countDown-- > 0)
            {
                return;
            }

            this._countDown = this._calmdown;

            if (Random.Range(0, 1f) > .3)
            {
                _PooledObject _currentGameObject = this._ObjectPool._Get();
            }
        }
    }
}
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Pool;

namespace _CA.Tests.LearningTests.TestCardPooling.UnityFeature
{
    public class _Card : MonoBehaviour, IPointerEnterHandler
    {
        private IObjectPool<_Card> _pool;
        public IObjectPool<_Card> _Pool
        {
            set => _pool = value;
        }

        private void OnEnable()
        {
            this.transform.position = new Vector3(
                Random.Range(-500f, 500f), 
                0f, 
                0.5f);
        }

        public void _Release()
        {
            this._pool.Release(this);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            this._Release();
        }
    }
}
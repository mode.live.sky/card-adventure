using UnityEngine;
using UnityEngine.Pool;

namespace _CA.Tests.LearningTests.TestCardPooling.UnityFeature
{
    public class _UnityPool : MonoBehaviour
    {
        [SerializeField] private _Card _cardPrefab;
        [SerializeField] private bool _collectionCheck = true;
        [SerializeField] private int _defaultCapacity = 5;
        [SerializeField] private int _maxCapacity = 20;

        public IObjectPool<_Card> _Pool;

        private void Awake()
        {
            this._Pool = new ObjectPool<_Card>(
                _onCreate,
                _onGet,
                _onRelease,
                _onDestroy,
                this._collectionCheck,
                this._defaultCapacity,
                this._maxCapacity);
        }
        private _Card _onCreate()
        {
            _Card instance = Instantiate(this._cardPrefab);
            instance._Pool = this._Pool;

            return instance;
        }
        private void _onGet(_Card card)
        {
            card.gameObject.SetActive(true);
        }

        private void _onRelease(_Card card)
        {
            card.gameObject.SetActive(false);
        }

        private void _onDestroy(_Card card)
        {
            Destroy(card.gameObject);
        }
    }
}
using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardPooling.UnityFeature
{
    public class _UnityPoolController : MonoBehaviour
    {
        [SerializeField] private _UnityPool _ObjectPool;
        [SerializeField] private float _calmdown = 50f;

        private float _countDown = 0f;

        private void FixedUpdate()
        {
            if (this._countDown-- > 0)
            {
                return;
            }

            this._countDown = this._calmdown;

            if (Random.Range(0, 1f) > .3)
            {
                _Card _currentGameObject = this._ObjectPool._Pool.Get();
            }
        }
    }
}
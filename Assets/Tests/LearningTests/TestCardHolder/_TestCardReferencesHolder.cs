using UnityEngine;
using UnityEngine.UI;

namespace _CA.Tests.LearningTests.TestCardHolder
{
    public class _TestCardReferencesHolder : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private Image _backgroundImage;

        public CanvasGroup _CanvasGroup => this._canvasGroup;
        public Image _BackgroundImage => this._backgroundImage;
    }
}
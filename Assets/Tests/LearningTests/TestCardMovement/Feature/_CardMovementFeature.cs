using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestCardMovement.Feature
{
    [RequireComponent(typeof(CanvasGroup))]
    public class _CardMovementFeature : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        private Camera _camera;
        private CanvasGroup _canvasGroup;

        private void Awake()
        {
            this._camera = Camera.main;
            this._canvasGroup = this.GetComponent<CanvasGroup>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {

            if (eventData?.pointerDrag == null)
            {
                Debug.Log($"Pointer drag is null.");
                return;
            }

            if (!Input.GetMouseButton(0))
            {
                Debug.Log($"Pointer clicked button isn't LMB.");
                return;
            }

            this.transform.position =
                this._camera.ScreenToWorldPoint(Input.mousePosition).With(z: 0);

            // or

            // Vector3 cursorPosition = this._camera.ScreenToWorldPoint(Input.mousePosition);
            // this.transform.position = new Vector3(cursorPosition.x, cursorPosition.y, 0f);
            // this.transform.position = cursorPosition.With(z: 0f);

            Debug.Log($"Dragged item = {eventData.pointerDrag}");
            Debug.Log($"Dragging to position = {this.transform.position}");

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = true;
        }
    }

    public static class _MyVector3Extension
    {
        public static Vector3 With(this Vector3 vector, float? x = null, float? y = null, float? z = null)
        {
            return new Vector3(x ?? vector.x, y ?? vector.y, z ?? vector.z);
        }
    }
}
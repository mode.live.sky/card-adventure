using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestCardMovement.Logic
{
    [RequireComponent(typeof(_ICard))]
    [RequireComponent(typeof(CanvasGroup))]
    public class _CardMovement : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        private _ICard _card;

        private Camera _camera;
        private CanvasGroup _canvasGroup;

        private bool _isMouseButtonLMBClicked => Input.GetMouseButton(0);


        private void Awake()
        {
            this._card = this.transform.GetComponent<_ICard>();

            this._camera = _CardsManager._Camera;
            this._canvasGroup = this.transform.GetComponent<CanvasGroup>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData?.pointerDrag == null)
            {
                return;
            }

            if (this._isMouseButtonLMBClicked)
            {
                _SetCardPositionBasedOnMousePointer();
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = true;
        }

        private void _SetCardPositionBasedOnMousePointer()
        {
            Vector3 cursorPosition = this._camera.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(cursorPosition.x, cursorPosition.y, 0f);
        }


    }
}

using UnityEditor;

using _CA.Tests.LearningTests.TestCardMovement.Logic;

namespace _CA.Tests.LearningTests.TestCardMovement.Editors
{
    [CustomEditor(typeof(_Card))]
    public class _CardEditor : Editor
    {
        public override void OnInspectorGUI()
        {
        }
    }
}
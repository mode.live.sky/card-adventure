using UnityEditor;

using _CA.Tests.LearningTests.TestCardMovement.Logic;

namespace _CA.Tests.LearningTests.TestCardMovement.Editors
{
    [CustomEditor(typeof(_CardsManager))]
    public class _CardsManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}
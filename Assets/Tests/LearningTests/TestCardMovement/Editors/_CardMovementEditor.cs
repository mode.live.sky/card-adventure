using UnityEditor;

using _CA.Tests.LearningTests.TestCardMovement.Logic;

namespace _CA.Tests.LearningTests.TestCardMovement.Editors
{
    [CustomEditor(typeof(_CardMovement))]
    public class _CardMovementEditor : Editor
    {
        public override void OnInspectorGUI()
        {
        }
    }
}
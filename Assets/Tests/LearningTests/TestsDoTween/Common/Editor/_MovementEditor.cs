﻿using UnityEngine;
using UnityEditor;

using Edit = UnityEditor.Editor;

namespace _CA.Tests.LearningTests.TestsDoTween.Common.Editor
{
    [CustomEditor(typeof(_Movement))]
    [CanEditMultipleObjects]
    public class _MovementEditor : Edit
    {
        public override void OnInspectorGUI()
        {
            GUILayout.Label($"Has own editor:\n{this.GetType()}");
        }
    }
}
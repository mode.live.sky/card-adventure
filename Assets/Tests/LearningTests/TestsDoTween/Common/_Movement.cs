using UnityEngine;
using UnityEngine.EventSystems;

using CovicDev.Extension;

namespace _CA.Tests.LearningTests.TestsDoTween.Common
{
    public class _Movement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private CanvasGroup _canvasGroup;

        private void Awake()
        {
            this._canvasGroup = this.transform.GetComponent<CanvasGroup>();
        }
        public void OnBeginDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!eventData?.pointerDrag)
            {
                return;
            }

            this.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition)._With(z: 0f);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = true;
        }
    }
}
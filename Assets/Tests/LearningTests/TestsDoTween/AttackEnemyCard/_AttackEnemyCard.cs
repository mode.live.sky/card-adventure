using UnityEngine;
using UnityEditor;

using DG.Tweening;
using CovicDev.Extension;

namespace _CA.Tests.LearningTests.TestsDoTween.AttackEnemyCard
{
    public class _AttackEnemyCard : MonoBehaviour
    {
        [SerializeField] private GameObject _enemyCard;
        [SerializeField] private float _durationOfAnimation;
        [SerializeField] private Ease _easeMode;

        private void OnGUI()
        {
            const int b = 70;
            if (GUI.Button(new Rect(b, b, b, b), "Attack"))
            {
                this._OrderToAttackEnemyCard();
            }
        }

        public void _OrderToAttackEnemyCard()
        {
            _AttackEnemyCardLogic attackEnemyCardLogic = new _AttackEnemyCardLogic(
                this.transform.gameObject,
                this._enemyCard,
                this._durationOfAnimation,
                this._easeMode
                );

            attackEnemyCardLogic._OrderToAttackEnemyCard();
        }

    }

    public class _AttackEnemyCardLogic
    {
        private GameObject _attackingCard;
        private GameObject _cardToAttack;
        private float _durationOfAnimation;
        private Ease _easeMode;

        private Vector3 _orignalPostion;

        public _AttackEnemyCardLogic(
            GameObject attackingCard,
            GameObject cardToAttack,
            float durationOfAnimation,
            Ease easeMode)
        {
            this._attackingCard = attackingCard;
            this._cardToAttack = cardToAttack;
            this._durationOfAnimation = durationOfAnimation;
            this._easeMode = easeMode;

            this._orignalPostion = attackingCard.transform.position._With(z: 0f);
        }

        public void _OrderToAttackEnemyCard()
        {
            Sequence animationSequence = DOTween.Sequence();

            animationSequence.Append(
            this._attackingCard.transform.DOMove(
                this._cardToAttack.transform.position._With(z: 0f),
                this._durationOfAnimation)
                .SetEase(this._easeMode)
            );

            animationSequence.Append(
                this._attackingCard.transform.DOMove(
                this._orignalPostion,
                this._durationOfAnimation)
                .SetEase(this._easeMode)
                );
        }
    }

    [CustomEditor(typeof(_AttackEnemyCard))]
    public class _AttackEnemyCardEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            _AttackEnemyCard attackEnemyCard = this.target as _AttackEnemyCard;

            base.DrawDefaultInspector();

            if (GUILayout.Button("Attack"))
            {
                attackEnemyCard._OrderToAttackEnemyCard();
            }
        }
    }
}
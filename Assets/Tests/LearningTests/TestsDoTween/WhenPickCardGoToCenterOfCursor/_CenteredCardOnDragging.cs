using System;

using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

using DG.Tweening;
using CovicDev.Extension;

namespace _CA.Tests.LearningTests.TestsDoTween.WhenPickCardGoToCenterOfCursor
{
    public class _CenteredCardOnDragging: MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [SerializeField] private float _duration = 0.5f;
        private CanvasGroup _canvasGroup;

        private void Awake()
        {
            this._canvasGroup = this.transform.GetComponent<CanvasGroup>();
        }


        public void OnDrag(PointerEventData eventData)
        {
            if (!eventData?.pointerDrag)
            {
                return;
            }

            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition)._With(z: 0f);

            this.transform
                .DOMove(position, _duration)
               //.SetEase(this._randomEase)
                .SetEase(Ease.Linear);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = false;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = true;
        }

        private Ease _randomEase
        {
            get
            {
                int value = Enum.GetNames(typeof(Ease)).Length;
                return (Ease)value._RandomIndex();
            }
        }
    }
}
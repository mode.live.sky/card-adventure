using UnityEngine;
using DG.Tweening;

using CovicDev.Extension;

namespace _CA.Tests.LearningTests.TestsDoTween.FeatureMove
{
    public class _DoTweenPileToHand : MonoBehaviour
    {

        [SerializeField] private Transform _endMoveObject;
        [SerializeField] private float _duration;
        [SerializeField] private Ease _ease;

        private void Start()
        {
            Vector3 parentPosition = _endMoveObject.transform.position;
            transform
                .DOMove(parentPosition._With(z: 0f), _duration)
                .SetEase(_ease);
        }

        private void OnGUI()
        {
            const int p = 100;
            if (GUI.Button(new Rect(p, p, p, p), "Go Back"))
            {
                Start();
            }
        }
    }
}
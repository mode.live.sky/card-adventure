namespace _CA.Tests.LearningTests.TestCardManager
{
    public interface _IHolder<out T>
    {
        T _Item { get; }
    }
}
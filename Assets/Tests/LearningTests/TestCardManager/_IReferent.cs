namespace _CA.Tests.LearningTests.TestCardManager
{
    public interface _IReferent<T> where T : _CardManagerHolder
    {
        T _Holder { get; }
        void _SetHolderReference(T holderReference);
    }
}
using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardManager
{
    [CreateAssetMenu(menuName = "_CA/_CardManager")]
    public class _CardManagerReferent : ScriptableObject, _IReferent<_CardManagerHolder>
    {
        public _CardManagerHolder _Holder { get; private set; }

        public void _SetHolderReference(_CardManagerHolder holderReference)
        {
            this._Holder = holderReference;
        }
    }
}
using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardManager
{
    public class _CardManagerCollector : MonoBehaviour
    {
        [SerializeField] private _CardManagerReferent _cardManagerReferent;

        [SerializeField] private _CardManager _cardManager;

        private _CardManagerHolder _cardManagerHolder;

        private void Awake()
        {
            this._cardManagerHolder = new _CardManagerHolder(this._cardManager);
            this._cardManagerReferent._SetHolderReference(this._cardManagerHolder);
        }
    }
}
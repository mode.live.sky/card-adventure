using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardManager
{
    public class _CardManagerHolder : _IHolder<_CardManager>
    {
        public _CardManager _CardManager { get; private set; }

        public _CardManager _Item { get; private set; }

        public _CardManagerHolder(_CardManager cardManager)
        {
            this._Item = cardManager;
            this._CardManager = cardManager;
        }
    }
}
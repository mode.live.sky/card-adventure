using UnityEngine;

namespace _CA.Tests.LearningTests.TestCardManager
{
    public class _CardManager : MonoBehaviour
    {
        public Camera _Camera { get; private set; }

        private void Awake()
        {
            this._Camera = Camera.main;
        }
    }
}
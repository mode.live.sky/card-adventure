using UnityEngine;
using UnityEngine.UI;

namespace _CA.Tests.LearningTests.TestCardManager
{
    public class _TestCardReferences : MonoBehaviour
    {
        [SerializeField] private _CardManagerReferent _cardManagerReferent;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private Image _backgroundImage;

        public CanvasGroup _CanvasGroup => _canvasGroup;
        public Image _BackgroundImage => _backgroundImage;

        public Camera _Camera => this._cardManagerReferent._Holder._CardManager._Camera;

        private void Start()
        {
            Debug.Log($"_Camera = {this._Camera}");
        }
    }
}
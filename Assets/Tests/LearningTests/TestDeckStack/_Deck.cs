using UnityEngine;
using System.Linq;

namespace _CA.Tests.LearningTests.TestDeckStack
{
    public class _Deck : MonoBehaviour
    {
        [SerializeField] private _DeckCardArray _deckCardArray;

        private static _DeckCardArray _sdeckCardArray;

        private void Awake()
        {
            _Deck._sdeckCardArray = this._deckCardArray;
        }

        public static _ACard _GetNextCard()
        {
            _ACard _card = _Deck._sdeckCardArray._GetRandomCard();
            _ACard instance = GameObject.Instantiate(_card);

            return instance;
        }
    }
}
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _CA.Tests.LearningTests.TestDeckStack
{
    [CreateAssetMenu(menuName = "_CA/_Tests/_DeckCards")]
    public class _DeckCardArray : ScriptableObject
    {
        [SerializeField] private List<_ACard> _decksCard;

        public int _Length
        {
            get
            {
                return this._decksCard.Count;
            }
        }

        public IEnumerable<_ACard> _GetDeck()
        {
            foreach (_ACard item in this._decksCard)
            {
                yield return item;
            }
        }

        public _ACard _GetRandomCard()
        {
            return this._decksCard[this._Length._RandIndex()];
        }
    }

    public static class _Extension
    {
        public static int _RandIndex(this int value)
        {
            return (int)Random.Range(0, value);
        }
    }
}
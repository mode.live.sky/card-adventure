using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestDeckStack
{
    public class _CardMovement : _ACard, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public void OnBeginDrag(PointerEventData eventData)
        {
            base._CanvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if(eventData?.pointerDrag == null)
            {
                return;
            }

            Vector3 position = this._Camera.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(position.x, position.y, 0f);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            base._CanvasGroup.blocksRaycasts = true;
        }
    }
}
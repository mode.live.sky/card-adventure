using UnityEngine;

public abstract class _ACard : MonoBehaviour, _ICard
{
    protected CanvasGroup _CanvasGroup;
    protected Camera _Camera;

    protected virtual void Awake()
    {
        this._CanvasGroup = this.transform.GetComponent<CanvasGroup>();
        this._Camera = Camera.main;
    }

    public virtual void _TransferCardTo(Transform parent)
    {
        this.transform.localPosition = parent.position;
    }
}

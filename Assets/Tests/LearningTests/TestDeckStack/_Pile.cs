using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestDeckStack
{
    public class _Pile : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            _ICard card =_Deck._GetNextCard();
            card._TransferCardTo(this.transform);
        }
    }
}
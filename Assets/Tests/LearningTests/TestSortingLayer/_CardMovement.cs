using UnityEngine;
using UnityEngine.EventSystems;

namespace _CA.Tests.LearningTests.TestSortingLayer
{
    public class _CardMovement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private CanvasGroup _canvasGroup;
        private void Awake()
        {
            this._canvasGroup = this.transform.GetComponent<CanvasGroup>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData?.pointerDrag is null)
            {
                return;
            }
            Vector3 postion = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(postion.x, postion.y, 0f);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            this._canvasGroup.blocksRaycasts = true;
        }
    }
}
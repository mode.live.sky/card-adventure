using Random = UnityEngine.Random;
using static UnityEngine.Mathf;

public static class _Extension
{
    public static int _RandIndex(this int value)
    {
        return (int)Ceil(Random.Range(0, value));
    }
}


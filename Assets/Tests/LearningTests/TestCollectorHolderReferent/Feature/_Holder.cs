namespace _CA.Tests.LearningTests.TestCollectorHolderReferent.Feature
{
    public class _Holder : _IHolder<_Item>
    {
        public _Item _Item { get; private set; }

        public _Holder(_Item item)
        {
            this._Item = item;
        }
    }
}
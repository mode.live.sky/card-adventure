using UnityEngine;

namespace _CA.Tests.LearningTests.TestCollectorHolderReferent.Feature
{
    public class _Item : MonoBehaviour
    {
        [SerializeField] private GameObject _sampleGameObject;

        public GameObject _SampleGameObject => this._sampleGameObject;

        private void Start()
        {
            Debug.Log($"_SampleGameObject = {this._SampleGameObject.name}");
        }
    }
}
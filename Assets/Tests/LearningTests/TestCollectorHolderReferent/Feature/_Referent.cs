using UnityEngine;

namespace _CA.Tests.LearningTests.TestCollectorHolderReferent.Feature
{
    [CreateAssetMenu(menuName = "_CA/_Tests/_Referent")]
    public class _Referent : ScriptableObject, _IReferent<_Holder>
    {
        public _Holder _Holder { get; private set; }

        public void _SetHolder(_Holder holder)
        {
            this._Holder = holder;
        }
    }
}
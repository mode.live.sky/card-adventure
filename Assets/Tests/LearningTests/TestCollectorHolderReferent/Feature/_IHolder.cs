namespace _CA.Tests.LearningTests.TestCollectorHolderReferent.Feature
{
    public interface _IHolder<out T>
    {
        T _Item { get; }
    }
}
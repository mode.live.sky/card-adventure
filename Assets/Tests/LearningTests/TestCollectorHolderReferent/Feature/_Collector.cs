using UnityEngine;

namespace _CA.Tests.LearningTests.TestCollectorHolderReferent.Feature
{
    public class _Collector : MonoBehaviour
    {
        [SerializeField] private _Referent _referent;
        [SerializeField] private _Item _item;

        private void Awake()
        {
            this._referent._SetHolder(new _Holder(this._item));
        }
    }
}
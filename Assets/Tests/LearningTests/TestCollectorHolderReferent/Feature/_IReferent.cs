namespace _CA.Tests.LearningTests.TestCollectorHolderReferent.Feature
{
    public interface _IReferent<T> where T : _Holder
    {
        T _Holder { get; }
        void _SetHolder(T holder);
    }
}